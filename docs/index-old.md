---
hide:
  - footer
---

# 🗃️ SOMMAIRE

<br />

!!! danger

    En cours de réécriture...


🏷️ [PRÉSENTATION](01-presentation)

🎓 [CRÉATION et GESTION de CLASSES](05-classes)

<br />

**Types d'activités**:

* 🧩 [PUZZLES](02-puzzles)

* 🤔 [DÉFIS](03-defis)

* 📃 [SUJETS (avec mode anti-triche & correction)](04-entrainements-devoirs)

<br /><br />

:fontawesome-solid-arrow-left: retour sur [**Code Puzzle**](https://www.codepuzzle.io/).

<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
